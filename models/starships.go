package models

import "database/sql"

type Starship struct {
	Name                 string         `json:"name"`
	Model                string         `json:"model"`
	Manufacturer         string         `json:"manufacturer"`
	CostInCredits        string         `json:"cost_in_credits" db:"cost_in_credits"`
	Length               string         `json:"length" db:"length"`
	MaxAtmospheringSpeed string         `json:"max_atmosphering_speed" db:"max_atmosphering_speed"`
	Crew                 string         `json:"crew" db:"crew"`
	Passengers           string         `json:"passengers"`
	CargoCapacity        string         `json:"cargo_capacity" db:"cargo_capacity"`
	Consumables          string         `json:"consumables"`
	HyperdriveRating     string         `json:"hyperdrive_rating" db:"hyperdrive_rating"`
	MGLT                 string         `json:"MGLT" db:"MGLT"`
	StarshipClass        string         `json:"starship_class" db:"starship_class"`
	Pilots               sql.NullString `json:"pilots"`
	Films                sql.NullString `json:"films"`
	FilmsArray           FilmsArray     `json:"-" db:-`
	Created              string         `json:"created"`
	Edited               string         `json:"edited"`
	URL                  string         `json:"url"`
	ID                   string         `json:"id"`
}

type Starships []Starship
