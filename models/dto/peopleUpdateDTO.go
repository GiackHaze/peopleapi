package dto

import "bitbucket.org/GiackHaze/peopleapi/jsonTypes"

// PeopleUpdateDTO : people update dto struct
type PeopleUpdateDTO struct {
	Name      jsonTypes.JSONNullString `json:"name"`
	Height    jsonTypes.JSONNullString `json:"height"`
	Mass      jsonTypes.JSONNullString `json:"mass"`
	HairColor jsonTypes.JSONNullString `json:"hair_color" db:"hair_color"`
	SkinColor jsonTypes.JSONNullString `json:"skin_color" db:"skin_color"`
	EyeColor  jsonTypes.JSONNullString `json:"eye_color" db:"eye_color"`
	BirthYear jsonTypes.JSONNullString `json:"birth_year" db:"birth_year"`
	Gender    jsonTypes.JSONNullString `json:"gender"`
	Homeworld jsonTypes.JSONNullString `json:"homeworld"`
	Films     jsonTypes.JSONNullString `json:"films"`
	Species   jsonTypes.JSONNullString `json:"species"`
	Vehicles  jsonTypes.JSONNullString `json:"vehicles"`
	Starships jsonTypes.JSONNullString `json:"starships"`
	Created   jsonTypes.JSONNullString `json:"created"`
	Edited    jsonTypes.JSONNullString `json:"edited"`
	URL       jsonTypes.JSONNullString `json:"url"`
	ID        jsonTypes.JSONNullString `json:"id"`
}
