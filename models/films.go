package models

type Films struct {
	title        string
	episodeID    string
	openingCrawl string
	director     string
	producer     string
	releaseDate  string
	characters   string
	planets      string
	starships    string
	vehicles     string
	species      string
	created      string
	edited       string
	url          string
	id           string
}

type FilmsArray []Films
