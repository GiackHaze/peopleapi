package models

import (
	"database/sql"
)

type Species struct {
	Name            string         `json:"name"`
	Classification  string         `json:"classification"`
	Designation     string         `json:"designation"`
	AverageHeight   string         `json:"average_height" db:"average_height"`
	SkinColors      string         `json:"skin_colors" db:"skin_colors"`
	HairColors      string         `json:"hair_colors" db:"hair_colors"`
	EyeColors       string         `json:"eye_colors" db:"eye_colors"`
	AverageLifespan string         `json:"average_lifespan" db:"average_lifespan"`
	Homeworld       string         `json:"homeworld" db:"homeworld"`
	Language        string         `json:"language" db:"language"`
	People          sql.NullString `json:"people" db:"people"`
	Films           sql.NullString `json:"films" db:"films"`
	FilmsArray      FilmsArray     `json:"-" db:"-"`
	Created         string         `json:"created" db:"created"`
	Edited          string         `json:"edited" db:"edited"`
	URL             string         `json:"url" db:"url"`
	ID              string         `json:"id" db:"id"`
}

type SpeciesArray []Species
