package validators

import "bitbucket.org/GiackHaze/peopleapi/models"

//People : Validate a people object before creating it in database
func People(p models.People) bool {
	return p.Name != ""
}
