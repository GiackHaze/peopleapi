package mappers

import (
	"bitbucket.org/GiackHaze/peopleapi/models"
	"bitbucket.org/GiackHaze/peopleapi/models/dto"
)

// PeopleMapping : function used to map a dto.PeopleUpdateDTO into a models.People
func PeopleMapping(dest *models.People, source dto.PeopleUpdateDTO) {
	if source.BirthYear.Valid {
		dest.BirthYear = source.BirthYear.String
	}
	if source.Created.Valid {
		dest.Created = source.Created.String
	}
	if source.Edited.Valid {
		dest.Edited = source.Edited.String
	}
	if source.EyeColor.Valid {
		dest.EyeColor = source.EyeColor.String
	}
	if source.Gender.Valid {
		dest.Gender = source.Gender.String
	}
	if source.HairColor.Valid {
		dest.HairColor = source.HairColor.String
	}
	if source.Height.Valid {
		dest.Height = source.Height.String
	}
	if source.Homeworld.Valid {
		dest.Homeworld = source.Homeworld.String
	}
	if source.Mass.Valid {
		dest.Mass = source.Mass.String
	}
	if source.Name.Valid {
		dest.Name = source.Name.String
	}
	if source.SkinColor.Valid {
		dest.SkinColor = source.SkinColor.String
	}
	if source.URL.Valid {
		dest.URL = source.URL.String
	}
}
