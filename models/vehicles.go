package models

import "database/sql"

type Vehicles struct {
	Name                 string         `json:"name"`
	Model                string         `json:"model"`
	Manufacturer         string         `json:"manufacturer"`
	CostInCredits        string         `json:"cost_in_credits" db:"cost_in_credits"`
	Length               string         `json:"length"`
	MaxAtmospheringSpeed string         `json:"max_atmosphering_speed" db:"max_atmosphering_speed"`
	Crew                 string         `json:"crew"`
	Passengers           string         `json:"passengers"`
	CargoCapacity        string         `json:"cargo_capacity" db:"cargo_capacity"`
	Consumables          string         `json:"consumables"`
	VehicleClass         string         `json:"vehicle_class" db:"vehicle_class"`
	Pilots               sql.NullString `json:"pilots"`
	Films                sql.NullString `json:"films"`
	Created              string         `json:"created"`
	Edited               string         `json:"edited"`
	URL                  string         `json:"url"`
	ID                   string         `json:"id"`
}

type VehiclesArray []Vehicles
