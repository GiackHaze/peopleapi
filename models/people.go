package models

import "database/sql"

type People struct {
	Name           string         `json:"name"`
	Height         string         `json:"height"`
	Mass           string         `json:"mass"`
	HairColor      string         `json:"hair_color" db:"hair_color"`
	SkinColor      string         `json:"skin_color" db:"skin_color"`
	EyeColor       string         `json:"eye_color" db:"eye_color"`
	BirthYear      string         `json:"birth_year" db:"birth_year"`
	Gender         string         `json:"gender"`
	Homeworld      string         `json:"homeworld"`
	Films          sql.NullString `json:"films"`
	Species        sql.NullString `json:"species"`
	SpeciesArray   SpeciesArray   `json:"species_array" db:-`
	Vehicles       sql.NullString `json:"vehicles"`
	VehiclesArray  VehiclesArray  `json:"vehicles_array" db:-`
	Starships      sql.NullString `json:"starships"`
	StarshipsArray Starships      `json:"starships_array" db:-`
	Created        string         `json:"created"`
	Edited         string         `json:"edited"`
	URL            string         `json:"url"`
	ID             string         `json:"id"`
}

type Peoples []People
