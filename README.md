### People API

The code in this repository has been written for a technical test.
I used sqlite3 driver from github.com/mattn/go-sqlite3 and github.com/jmoiron/sqlx to spend less time parsing results, everything else is made with standard libs.

The API can handle GET, POST, PUT, PATCH, and DELETE requests on /people which enables you ro perform standard operations on "people" table in the database.
As it was asked, when you get a people you get the list of starships, vehicles and species (as a bonus) with it in fields called respectively starships_array, vehicles_array, and species_array. Other operations such as retrieving other entities or adding relationships between people and other entities are not implemented as the goal of this exercise is to show my skills more than building a full functional API.

For the same reason evoked just before, there is only one test file just to show that I do know that a functionnality is not done until it's not tested but, again, it's an exercise. Same thing goes for comments in models.

If you have any questions on this you can contact me at giackhaze@gmail.com .

## Install

# Method 1
- Launch `go get bitbucket.org/GiackHaze/peopleapi` in an environment with Go installed.

# Method 2
- Clone the repository with `git clone`
- Launch `Go install` in the directory created by the previous command

## Launch the program

- If your GOBIN directory is in your PATH, simply launch `peopleapi "path/to/sqlite3/database"`

- If it's not the case:
  - `cd $GOBIN`
  - `peopleapi "path/to/sqlite3/database"`

- Other option: go into the folder src/bitbucket.org/GiackHaze/peopleapi of your Go workspace and launch `go run main.go`

## Some testing examples with curl
- `curl localhost:8999/people/1 -v`
- `curl localhost:8999/people -v`
- `curl localhost:8999/people -d '{"name":"testAPI"}' -v`
- `curl -XPUT localhost:8999/people/90 -d '{"name":"testUpdate", "homeworld":"updateHomeWorld"}' -v`
- `curl -XDELETE localhost:8999/people/90 -v`
- `curl -XPATCH localhost:8999/people/90 -d '{"name":"testPatch"}'`
- `curl -XPATCH localhost:8999/people/99 -d '{"name":"testPatchOnNotFoundID"}'`
