package main

import (
	"fmt"
	"net/http"
	"os"

	"bitbucket.org/GiackHaze/peopleapi/handlers"

	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"
)

func main() {
	pathToDatabase := "./swapi.dat"
	if len(os.Args) > 1 {
		pathToDatabase = os.Args[1]
	}
	database, err := sqlx.Open("sqlite3", pathToDatabase)
	if err != nil {
		panic("Erreur durant la connexion à la BDD.")
	}
	defer database.Close()
	handlers.Database = database
	http.HandleFunc("/people", handlers.PeopleHandler)
	http.HandleFunc("/people/", handlers.PeopleHandler)
	fmt.Println("listening on port 8999")
	err2 := http.ListenAndServe(":8999", nil)
	if err != nil {
		panic(err2)
	}
}
