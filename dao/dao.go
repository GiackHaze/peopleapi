package dao

import (
	"errors"
	"fmt"
	"strconv"

	"bitbucket.org/GiackHaze/peopleapi/models"

	"github.com/jmoiron/sqlx"
)

func GetPeopleStarships(people models.People, Database *sqlx.DB) models.Starships {
	relations, err := Database.Query("select starships from people_starships where people=" + people.ID)
	defer relations.Close()
	if err != nil {
		fmt.Println("Error while executing statement.")
		fmt.Println(err)
	}
	var result []models.Starship
	for relations.Next() {
		var starship string
		relations.Scan(&starship)
		var starshipEntity []models.Starship
		err := Database.Select(&starshipEntity, "select * from starships where id="+starship)
		if err != nil {
			fmt.Println("Error in select.")
			fmt.Print(err)
		} else {
			if len(starshipEntity) > 0 {
				result = append(result, starshipEntity[0])
			}
		}
	}
	return models.Starships(result)
}

func GetPeopleVehicles(people models.People, Database *sqlx.DB) models.VehiclesArray {
	relations, err := Database.Query("select vehicles from people_vehicles where people=" + people.ID)
	defer relations.Close()
	if err != nil {
		fmt.Println("Error while executing statement.")
		fmt.Println(err)
	}
	var result []models.Vehicles
	for relations.Next() {
		var vehicle string
		relations.Scan(&vehicle)
		var vehicleEntity []models.Vehicles
		err := Database.Select(&vehicleEntity, "select * from vehicles where id="+vehicle)
		if err != nil {
			fmt.Println("Error in select.")
			fmt.Print(err)
		} else {
			if len(vehicleEntity) > 0 {
				result = append(result, vehicleEntity[0])
			}
		}
	}
	return models.VehiclesArray(result)
}

func GetPeopleSpecies(people models.People, Database *sqlx.DB) models.SpeciesArray {
	relations, err := Database.Query("select species from people_species where people=" + people.ID)
	defer relations.Close()
	if err != nil {
		fmt.Println("Error while executing statement.")
		fmt.Println(err)
	}
	var result []models.Species
	for relations.Next() {
		var species string
		relations.Scan(&species)
		var speciesEntity []models.Species
		err := Database.Select(&speciesEntity, "select * from species where id="+species)
		if err != nil {
			fmt.Println("Error in select.")
			fmt.Println(err)
		} else {
			if len(speciesEntity) > 0 {
				result = append(result, speciesEntity[0])
			}
		}
	}
	return models.SpeciesArray(result)
}

func GetAllPeople(Database *sqlx.DB) []models.People {
	rows, err := Database.Queryx("SELECT * FROM people")
	defer rows.Close()
	if err != nil {
		fmt.Println("Error in select.")
		fmt.Print(err)
	}
	var peopleTab []models.People
	for rows.Next() {
		people := models.People{}
		err := rows.StructScan(&people)
		if err != nil {
			fmt.Println("Error in scan.")
			fmt.Print(err)
		}
		peopleTab = append(peopleTab, people)
	}
	return peopleTab
}

func GetPeopleByID(Database *sqlx.DB, id string) (models.People, error) {
	rows, err := Database.Queryx("select * from people where id=" + id)
	defer rows.Close()
	if err != nil {
		fmt.Println("Error in select.")
		fmt.Print(err)
	}
	var peopleTab []models.People
	for rows.Next() {
		people := models.People{}
		err := rows.StructScan(&people)
		if err != nil {
			fmt.Println("Error in scan.")
			fmt.Print(err)
		}
		peopleTab = append(peopleTab, people)
	}
	if len(peopleTab) == 0 {
		fmt.Println("People " + id + " not found.")
		return models.People{}, errors.New("People " + id + " not found.")
	}
	return peopleTab[0], nil
}

func GetNextPossibleID(Database *sqlx.DB) int {
	rows, err := Database.Queryx("SELECT id FROM people")
	defer rows.Close()
	if err != nil {
		fmt.Println("Error in select.")
		fmt.Print(err)
	}
	var id int
	for rows.Next() {
		var tmp string
		rows.Scan(&tmp)
		numericalTmp, _ := strconv.Atoi(tmp)
		if id < numericalTmp {
			id = numericalTmp
		}
	}
	fmt.Println(id)
	return id + 1
}

func InsertPeople(Database *sqlx.DB, people models.People) error {
	transaction := Database.MustBegin()
	_, err := transaction.NamedExec(getPeopleInsertString(), &people)
	if err != nil {
		fmt.Println("Error while executing request.")
		return err
	}
	err = transaction.Commit()
	if err != nil {
		fmt.Println("Error while committing transaction.")
		return err
	}
	return nil
}

func DeletePeopleByID(Database *sqlx.DB, id string) error {
	res, err := Database.Exec("delete from people where id=" + id)
	if err != nil {
		return err
	}
	if count, _ := res.RowsAffected(); count < 1 {
		return errors.New("People not found.")
	}
	return nil
}

func getPeopleInsertString() string {
	attributesList := "name, height, mass, hair_color, skin_color, eye_color, birth_year, gender, homeworld, films, species, vehicles, starships, created, edited, url, id"
	parametersList := ":name, :height, :mass, :hair_color, :skin_color, :eye_color, :birth_year, :gender, :homeworld, :films, :species, :vehicles, :starships, :created, :edited, :url, :id"
	insertString := "insert into people (" + attributesList + ") values(" + parametersList + ")"
	return insertString
}
