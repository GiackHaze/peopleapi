package handlers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"

	"bitbucket.org/GiackHaze/peopleapi/dao"
	"bitbucket.org/GiackHaze/peopleapi/models"
	"bitbucket.org/GiackHaze/peopleapi/models/dto"
	"bitbucket.org/GiackHaze/peopleapi/models/mappers"
	"bitbucket.org/GiackHaze/peopleapi/models/validators"

	"github.com/jmoiron/sqlx"
)

// Database connection shared between handlers, to be instantiated in the main process
var Database *sqlx.DB

// PeopleHandler a handler for people model
func PeopleHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		fmt.Println(r.Method + ": " + r.URL.String())
		splittedURL := strings.Split(r.URL.String(), "/")
		if len(splittedURL) == 3 {
			if splittedURL[2] == "" {
				http.Error(w, "The url is malformed.", http.StatusBadRequest)
				return
			}
			handleGetByID(w, r, Database, splittedURL[2])
		} else {
			handleGet(w, r, Database)
		}
	case "POST":
		fmt.Println(r.Method)
		handlePost(w, r, Database)
	case "PUT":
		fmt.Println(r.Method)
		splittedURL := strings.Split(r.URL.String(), "/")
		if len(splittedURL) == 3 {
			handlePut(w, r, Database, splittedURL[2])
		} else {
			http.Error(w, "The PUT verb is authorised only on /people/id .", http.StatusMethodNotAllowed)
		}
	case "PATCH":
		fmt.Println(r.Method)
		splittedURL := strings.Split(r.URL.String(), "/")
		if len(splittedURL) == 3 {
			handlePatch(w, r, Database, splittedURL[2])
		} else {
			http.Error(w, "The PATCH verb is authorised only on /people/id .", http.StatusMethodNotAllowed)
		}
	case "DELETE":
		fmt.Println(r.Method)
		splittedURL := strings.Split(r.URL.String(), "/")
		if len(splittedURL) == 3 {
			handleDelete(w, r, Database, splittedURL[2])
		} else {
			http.Error(w, "The DELETE verb is authorised only on /people/id .", http.StatusMethodNotAllowed)
		}
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}

func handleGet(w http.ResponseWriter, r *http.Request, Database *sqlx.DB) {
	peopleTab := dao.GetAllPeople(Database)
	for index := range peopleTab {
		peopleTab[index].StarshipsArray = dao.GetPeopleStarships(peopleTab[index], Database)
		peopleTab[index].SpeciesArray = dao.GetPeopleSpecies(peopleTab[index], Database)
		peopleTab[index].VehiclesArray = dao.GetPeopleVehicles(peopleTab[index], Database)
	}
	res, err := json.Marshal(models.Peoples(peopleTab))
	if err != nil {
		fmt.Println("Error while marshalling.")
		fmt.Println(err)
		http.Error(w, "Error while marshalling.", http.StatusInternalServerError)
		return
	}
	w.Write(res)
}

func handleGetByID(w http.ResponseWriter, r *http.Request, Database *sqlx.DB, id string) {
	people, err := dao.GetPeopleByID(Database, id)
	if err != nil {
		fmt.Println(err)
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
	people.StarshipsArray = dao.GetPeopleStarships(people, Database)
	people.SpeciesArray = dao.GetPeopleSpecies(people, Database)
	people.VehiclesArray = dao.GetPeopleVehicles(people, Database)
	res, err := json.Marshal(people)
	if err != nil {
		fmt.Println("Error while marshalling.")
		fmt.Println(err)
	}
	w.Write(res)
}

func handlePost(w http.ResponseWriter, r *http.Request, Database *sqlx.DB) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Println("Error while reading body.")
		fmt.Println(err)
	}
	var people models.People
	err = json.Unmarshal(body, &people)
	if err != nil {
		fmt.Println("Error while reading body.")
		fmt.Println(err)
	}
	if !validators.People(people) {
		fmt.Println("Invalid Payload.")
		http.Error(w, "You should define at least a name to cerate a people entity.", http.StatusBadRequest)
		return
	}
	people.ID = strconv.Itoa(dao.GetNextPossibleID(Database))
	fmt.Println(people)
	err = dao.InsertPeople(Database, people)
	if err != nil {
		fmt.Println("Error while inserting people.")
		fmt.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusCreated)
}

func handlePut(w http.ResponseWriter, r *http.Request, Database *sqlx.DB, id string) {
	peopleEntity, err := dao.GetPeopleByID(Database, id)
	if err != nil {
		fmt.Print(err)
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
	handlePutInternal(w, r, Database, peopleEntity)
}

func handlePutInternal(w http.ResponseWriter, r *http.Request, Database *sqlx.DB, peopleEntity models.People) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Println("Error while reading body.")
		fmt.Println(err)
	}
	var people dto.PeopleUpdateDTO
	err = json.Unmarshal(body, &people)
	if err != nil {
		fmt.Println("Error unmarshalling body.")
		fmt.Println(err)
	}

	mappers.PeopleMapping(&peopleEntity, people)
	if !validators.People(peopleEntity) {
		fmt.Println("Invalid Payload.")
		http.Error(w, "You should define at least a name to cerate a people entity.", http.StatusBadRequest)
		return
	}

	err = dao.DeletePeopleByID(Database, peopleEntity.ID)
	if err != nil {
		fmt.Println("Error while deleting people.")
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	err = dao.InsertPeople(Database, peopleEntity)
	if err != nil {
		fmt.Println("Error inserting people.")
		fmt.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusCreated)
	res, err := json.Marshal(peopleEntity)
	if err != nil {
		fmt.Println("Error while marshalling.")
		fmt.Println(err)
	}
	w.Write(res)
}

func handlePatch(w http.ResponseWriter, r *http.Request, Database *sqlx.DB, id string) {
	peopleEntity, err := dao.GetPeopleByID(Database, id)
	if err != nil {
		handlePost(w, r, Database)
	} else {
		handlePutInternal(w, r, Database, peopleEntity)
	}
}

func handleDelete(w http.ResponseWriter, r *http.Request, Database *sqlx.DB, id string) {
	err := dao.DeletePeopleByID(Database, id)
	if err != nil {
		fmt.Println("Error while deleting people.")
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
	w.WriteHeader(http.StatusNoContent)
}
