package jsonTypes

import (
	"database/sql"
	"encoding/json"
)

// JSONNullString : a wrapper implementing json.Marshal and json.Unmarshal interface
type JSONNullString struct {
	sql.NullString
}

// MarshalJSON : method to implement for json.Marshal
func (v JSONNullString) MarshalJSON() ([]byte, error) {
	if v.Valid {
		return json.Marshal(v.String)
	}
	return json.Marshal(nil)
}

// UnmarshalJSON : method to implement for json.Unmarshal
func (v *JSONNullString) UnmarshalJSON(data []byte) error {
	// Unmarshalling into a pointer will let us detect null
	var x *string
	if err := json.Unmarshal(data, &x); err != nil {
		return err
	}
	if x != nil {
		v.Valid = true
		v.String = *x
	} else {
		v.Valid = false
	}
	return nil
}
